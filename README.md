hspear
------

[Haskell](http://haskell.org/) parser for
[Spear](http://www.klingbeil.com/spear/) analysis files.

## cli

[util](?t=hspear&e=md/util.md)

© [rohan drape](http://rohandrape.net/),
  2012-2024,
  [gpl](http://gnu.org/copyleft/)
