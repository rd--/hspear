module Sound.Analysis.Spear.Ptpf.Reduce where

import qualified Music.Theory.Tuple as Tuple {- hmt-base -}

import qualified Sound.Sc3.Common.Math as Sc3 {- hsc3 -}

import qualified Sound.Analysis.Spear.Ptpf as Ptpf {- hspear -}

-- * Plain

type R = Double

node_to_t3 :: Ptpf.Node -> Tuple.T3 R
node_to_t3 (Ptpf.Node _ tm fr am) = (tm, fr, am)

seq_to_t3 :: Ptpf.Seq -> [Tuple.T3 R]
seq_to_t3 = map node_to_t3 . Ptpf.s_data

t3_plain_reduce :: (R, R) -> [Tuple.T3 R] -> [Tuple.T3 R]
t3_plain_reduce (freq_diff, amp_diff) =
  let f _ [x] = [x]
      f _ [] = []
      f (tm', fr', am') ((tm, fr, am) : xs) =
        if Sc3.absdif fr fr' > freq_diff || Sc3.absdif am am' > amp_diff
          then (tm, fr, am) : f (tm, fr, am) xs
          else f (tm', fr', am') xs
      g (x : xs) = x : f x xs
      g [] = []
  in g

-- * Gradient

-- | True if (n1,n2,n3) can be reduced to (n1,n3).
type N_Reduction_F = ((Ptpf.Node, Ptpf.Node, Ptpf.Node) -> Bool)

n_reduction :: N_Reduction_F -> [Ptpf.Node] -> [Ptpf.Node]
n_reduction f n =
  case n of
    n1 : n2 : n3 : n' ->
      if f (n1, n2, n3)
        then n_reduction f (n1 : n3 : n')
        else n1 : n_reduction f (n2 : n3 : n')
    _ -> n

s_reduction :: N_Reduction_F -> Ptpf.Seq -> Ptpf.Seq
s_reduction f (Ptpf.Seq i s e _ d) =
  let d' = n_reduction f d
  in Ptpf.Seq i s e (length d') d'

-- | Frequency (Fmidi) and amplitude (DB) gradient from /n1/ to /n2/.
n_gradient :: Ptpf.Node -> Ptpf.Node -> (Ptpf.N_Data, Ptpf.N_Data)
n_gradient (Ptpf.Node _ t1 f1 a1) (Ptpf.Node _ t2 f2 a2) =
  let dt = realToFrac (t2 - t1)
  in ( (Sc3.cps_to_midi f2 - Sc3.cps_to_midi f1) / dt
     , (Sc3.amp_to_db a2 - Sc3.amp_to_db a1) / dt
     )

s_reduction_gradient :: (Ptpf.N_Data, Ptpf.N_Data) -> Ptpf.Seq -> Ptpf.Seq
s_reduction_gradient (p, q) =
  let f (n1, n2, n3) =
        let (a, b) = n_gradient n1 n2
            (c, d) = n_gradient n1 n3
        in abs (a - c) < p && abs (b - d) < q
  in s_reduction f

p_reduction_gradient :: (Ptpf.N_Data, Ptpf.N_Data) -> Ptpf.Ptpf -> Ptpf.Ptpf
p_reduction_gradient g (Ptpf.Ptpf n s) = Ptpf.Ptpf n (map (s_reduction_gradient g) s)
