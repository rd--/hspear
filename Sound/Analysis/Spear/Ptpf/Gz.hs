-- | Variants for handling @GZip@ compressed data.
module Sound.Analysis.Spear.Ptpf.Gz where

import System.FilePath {- filepath -}

import qualified Codec.Compression.GZip as GZip {- zlib -}
import qualified Data.ByteString.Lazy.Char8 as ByteString {- bytestring -}

import Sound.Analysis.Spear.Ptpf {- hspear -}

-- | Variant of 'parse_ptpf' running 'GZip.decompress'.
parse_ptpf_gz :: ByteString.ByteString -> Either String Ptpf
parse_ptpf_gz = parse_ptpf . GZip.decompress

-- | Load compressed Spear data.
load_ptpf_gz :: FilePath -> IO (Either String Ptpf)
load_ptpf_gz = fmap parse_ptpf_gz . ByteString.readFile

-- | Load either compressed or uncompressed Spear data.
load_ptpf_maybe_gz :: FilePath -> IO (Either String Ptpf)
load_ptpf_maybe_gz fn =
  if takeExtension fn == ".gz"
    then load_ptpf_gz fn
    else load_ptpf fn

-- | Apply /f/ at 'Right', else 'id'.
at_right :: (a -> b) -> Either t a -> Either t b
at_right f = either (Left . id) (Right . f)

-- | Variant of 'load_ptpf_gz' transforming with 'ptpf_time_asc'.
load_ptpf_gz_time_asc :: FilePath -> IO (Either String [(N_Time, [Node])])
load_ptpf_gz_time_asc = fmap (at_right ptpf_time_asc) . load_ptpf_gz
