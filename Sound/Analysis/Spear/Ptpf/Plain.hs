{- | /Spear/ @Ptpf@ (par-text-partials-format) files.

Plain representation of data as stored in file.
-}
module Sound.Analysis.Spear.Ptpf.Plain where

import qualified Data.ByteString.Lazy.Char8 as ByteString {- bytestring -}
import qualified Data.ByteString.Lex.Fractional as Parser {- bytestring-lexing -}
import qualified Data.Map as Map {- containers -}
import qualified Data.Vector.Storable as Vector {- vector -}

import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Tuple as Tuple {- hmt-base -}

-- * Partial

data Partial = Partial
  { p_id :: Int
  , p_size :: Int -- number of (time:sec,freq:hz,ampl:linear) triples
  , p_start_time :: Float
  , p_end_time :: Float
  , p_data :: Vector.Vector Float
  }
  deriving (Eq, Show)

type Node = (Float, Float, Float)

-- | Lookup /kth/ (time,freq,ampl) triple at /p/.
p_node_at :: Partial -> Int -> Node
p_node_at p k =
  let f = (Vector.!) (p_data p)
      m = k * 3
  in (f m, f (m + 1), f (m + 2))

-- | Variant that checks /k/ is valid.
p_node_at_err :: Partial -> Int -> Node
p_node_at_err p k =
  if k < 0 || k >= p_size p
    then error "p_node: k < 0 || k > size"
    else p_node_at p k

type Seg = (Node, Node)

p_seg_at :: Partial -> Int -> Seg
p_seg_at p k = (p_node_at p k, p_node_at p (k + 1))

p_seg_at_err :: Partial -> Int -> Seg
p_seg_at_err p k =
  if k < 0 || k >= p_size p - 1
    then error "p_node: k < 0 || k > size - 1"
    else p_seg_at p k

-- * Ptpf

data Ptpf = Ptpf
  { ptpf_size :: Int -- number of partials
  , ptpf_data :: Map.Map Int Partial
  }
  deriving (Eq, Show)

ptpf_data_list :: Ptpf -> [Partial]
ptpf_data_list ptpf = map snd (Map.toList (ptpf_data ptpf))

-- * Parser

type Str = ByteString.ByteString

str_int :: ByteString.ByteString -> Int
str_int = maybe 0 fst . ByteString.readInt

str_float :: ByteString.ByteString -> Float
str_float = maybe 0 fst . Parser.readSigned Parser.readDecimal . ByteString.toStrict

str_double :: ByteString.ByteString -> Double
str_double = maybe 0 fst . Parser.readSigned Parser.readDecimal . ByteString.toStrict

str_words :: ByteString.ByteString -> [ByteString.ByteString]
str_words = ByteString.split ' '

str_lines :: ByteString.ByteString -> [ByteString.ByteString]
str_lines = filter (not . ByteString.null) . ByteString.split '\n'

-- | Parse 'Partial' from pair of input lines.
ptpf_parse_partial :: (Str, Str) -> Partial
ptpf_parse_partial (i, j) =
  let (ix, n, st, et) = Tuple.t4_from_list (str_words i)
      v = Vector.fromList (map str_float (str_words j))
  in if str_int n * 3 /= Vector.length v
      then error "ptpf_seq"
      else Partial (str_int ix) (str_int n) (str_float st) (str_float et) v

-- | Parse header section, result is number of partials.
ptpf_parse_header :: [Str] -> Maybe Int
ptpf_parse_header h =
  let mk = ByteString.pack
      r0 = mk "par-text-partials-format"
      r1 = mk "point-type time frequency amplitude"
      r2 = mk "partials-count "
      r3 = mk "partials-data"
  in case h of
      [h0, h1, h2, h3] ->
        if h0 == r0 && h1 == r1 && h3 == r3
          then Just (str_int (ByteString.drop (ByteString.length r2) h2))
          else Nothing
      _ -> Nothing

{- | Parse 'Ptpf' at 'Str'.

Partial Id's are ascending from zero, error if not.
-}
ptpf_parse :: Str -> Either String Ptpf
ptpf_parse s =
  let l = str_lines s
      (h, d) = splitAt 4 l
      jn k p = if p_id p /= k then error "ptpf_parse: non-ascending?" else (k, p)
  in case ptpf_parse_header h of
      Just np ->
        let p_seq = map ptpf_parse_partial (List.adj2 2 d)
        in if length p_seq /= np
            then Left ("ptpf_parse: partial count: " ++ show (np, length p_seq))
            else Right (Ptpf np (Map.fromList (zipWith jn [0 ..] p_seq)))
      _ -> Left "ptpf_parse: illegal header"

-- | Load Ptpf data.
ptpf_load :: FilePath -> IO (Either String Ptpf)
ptpf_load = fmap ptpf_parse . ByteString.readFile

-- | Erroring variant of 'ptpf_load'.
ptpf_load_err :: FilePath -> IO Ptpf
ptpf_load_err = fmap (either error id) . ptpf_load
