-- | /Spear/ @Ptpf@ (par-text-partials-format) files.
module Sound.Analysis.Spear.Ptpf where

import Control.Monad {- base -}
import Data.Function {- base -}
import Data.List {- base -}
import qualified Data.List.Ordered as O {- data-ordlist -}
import Data.Maybe {- base -}

import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Math as Math {- hmt-base -}
import qualified Music.Theory.Math.Convert as Math.Convert {- hmt-base -}
import qualified Music.Theory.Tuple as Tuple {- hmt-base -}

import qualified Sound.Sc3.Common.Math.Interpolate as Sc3 {- hsc3 -}

import qualified Sound.Analysis.Spear.Ptpf.Plain as Plain

-- * Node

-- | Timestamps are floating point.
type N_Time = Double

-- | Data points are floating point.
type N_Data = Double

-- | Record to hold data for single node of a partial track.
data Node = Node
  { n_partial_id :: Int
  -- ^ Partial identifier
  , n_time :: N_Time
  , n_frequency :: N_Data
  , n_amplitude :: N_Data
  }
  deriving (Eq, Show)

-- | Set 'n_amplitude' at 'Node' to @0@.
n_zero_amplitude :: Node -> Node
n_zero_amplitude e = e {n_amplitude = 0}

-- | Set 'n_partial_id' at 'Node'.
n_set_partial_id :: Int -> Node -> Node
n_set_partial_id k e = e {n_partial_id = k}

-- | Apply transform /f/ at 'n_time'.
n_temporal_f :: (N_Time -> N_Time) -> Node -> Node
n_temporal_f f e = e {n_time = f (n_time e)}

-- | Find the nodes that window (i.e lie on either side of) the indicated time.
n_at :: N_Time -> [Node] -> Maybe (Node, Node)
n_at tm nd =
  case nd of
    [] -> Nothing
    [_] -> Nothing
    p : q : nd' -> if n_time p <= tm && n_time q > tm then Just (p, q) else n_at tm (q : nd')

-- | Interpolation between two nodes, exponential for frequency and linear for amplitude.
n_interp :: N_Time -> (Node, Node) -> Maybe Node
n_interp ix (Node k tm fr am, Node k' tm' fr' am') =
  if tm <= ix && tm' > ix && k == k'
    then
      let r = (ix - tm) / (tm' - tm)
      in Just (Node k ix (Sc3.exponential fr fr' r) (Sc3.linear am am' r))
    else Nothing

-- * Seq

-- | A sequence of partial 'Node' data.
data Seq = Seq
  { s_identifier :: Int
  -- ^ '==' to 'n_partial_id' at 's_data'.
  , s_start_time :: N_Time
  -- ^ 'minimum' 'n_time' at 's_data'.
  , s_end_time :: N_Time
  -- ^ 'maximum' 'n_time' at 's_data'.
  , s_nodes :: Int
  -- ^ '==' to 'length' 's_data'
  , s_data :: [Node]
  }
  deriving (Eq, Show)

-- | Apply /f/ at 's_data' of 'Seq' and re-calculate temporal bounds.
s_map :: (Node -> Node) -> Seq -> Seq
s_map f (Seq i _ _ n d) =
  let d' = map f d
      (s, e) = List.minmax (map n_time d')
  in Seq i s e n d'

-- | Find the nodes that window indicated time.
s_at :: N_Time -> Seq -> Maybe (Node, Node)
s_at tm (Seq _ st en _ nd) =
  if tm >= st && tm <= en
    then n_at tm nd
    else Nothing

-- | 'n_interp' of 's_at'.
s_interp :: N_Time -> Seq -> Maybe Node
s_interp tm = join . fmap (n_interp tm) . s_at tm

-- | Apply /g/ at each 'Node' of 'Seq', and /f/ to the result.
s_summarise :: ([a] -> b) -> (Node -> a) -> Seq -> b
s_summarise f g = f . map g . s_data

-- | 'maximum' 'n_amplitude' at 's_data'.
s_max_amplitude :: Seq -> N_Data
s_max_amplitude = s_summarise maximum n_amplitude

-- | 'minimum' 'n_amplitude' at 's_data'.
s_min_amplitude :: Seq -> N_Data
s_min_amplitude = s_summarise minimum n_amplitude

-- | 'mean' 'n_amplitude' at 's_data'.
s_mean_amplitude :: Seq -> N_Data
s_mean_amplitude = s_summarise Math.ns_mean_of_list n_amplitude

-- | 'minimum' 'n_frequency' at 's_data'.
s_min_frequency :: Seq -> N_Data
s_min_frequency = s_summarise minimum n_frequency

-- | 'maximum' 'n_frequency' at 's_data'.
s_max_frequency :: Seq -> N_Data
s_max_frequency = s_summarise maximum n_frequency

-- | 'mean' 'n_frequency' at 's_data'.
s_mean_frequency :: Seq -> N_Data
s_mean_frequency = s_summarise Math.ns_mean_of_list n_frequency

-- | 's_end_time' '-' 's_start_time'.
s_duration :: Seq -> N_Time
s_duration s = s_end_time s - s_start_time s

-- | Set 's_identifier' and associated 'n_partial_id'.
s_set_identifier :: Int -> Seq -> Seq
s_set_identifier k s =
  s
    { s_identifier = k
    , s_data = map (n_set_partial_id k) (s_data s)
    }

-- | '==' 'on' 's_identifier'.
s_eq_identifier :: Seq -> Seq -> Bool
s_eq_identifier = (==) `on` s_identifier

-- | 'unionBy' 's_eq_identifier'.
s_union :: [Seq] -> [Seq] -> [Seq]
s_union = unionBy s_eq_identifier

-- | Apply transform /f/ at 'n_time'.
s_temporal_f :: (N_Time -> N_Time) -> Seq -> Seq
s_temporal_f f s =
  let (Seq i st et n d) = s
  in Seq i (f st) (f et) n (map (n_temporal_f f) d)

-- * Ptpf

-- | A 'Ptpf' is a set of 'Seq'.
data Ptpf = Ptpf
  { p_partials :: Int
  , p_seq :: [Seq]
  }
  deriving (Eq, Show)

-- | 'minimum' 's_start_time' at 'p_seq'.
p_start_time :: Ptpf -> N_Time
p_start_time = minimum . map s_start_time . p_seq

-- | 'maximum' 's_end_time' at 'p_seq'.
p_end_time :: Ptpf -> N_Time
p_end_time = maximum . map s_end_time . p_seq

-- | 'sum' of 's_nodes' of 'p_seq'.
p_nodes :: Ptpf -> Int
p_nodes = sum . map s_nodes . p_seq

-- | Generate 'Ptpf' from set of 'Seq'.  Re-assigns partial identifiers.
p_from_seq :: [Seq] -> Ptpf
p_from_seq s =
  let n = length s
      s' = zipWith s_set_identifier [0 ..] s
  in Ptpf n s'

p_temporal_f :: (N_Time -> N_Time) -> Ptpf -> Ptpf
p_temporal_f f (Ptpf n s) = Ptpf n (map (s_temporal_f f) s)

p_map :: (Seq -> Seq) -> Ptpf -> Ptpf
p_map f (Ptpf n s) = Ptpf n (map f s)

p_filter :: (Seq -> Bool) -> Ptpf -> Ptpf
p_filter f (Ptpf _ s) =
  let s' = filter f s
  in Ptpf (length s') s'

p_node_map :: (Node -> Node) -> Ptpf -> Ptpf
p_node_map f = p_map (s_map f)

p_interp :: N_Time -> Ptpf -> [Node]
p_interp tm = mapMaybe (s_interp tm) . p_seq

-- | Calculate extent of 'Ptpf' as (time,freq,ampl) minima and maxima.
p_extent :: Ptpf -> (Tuple.T2 N_Time, Tuple.T2 N_Data, Tuple.T2 N_Data)
p_extent p =
  let sq = p_seq p
      nd = concatMap s_data sq
  in ( (p_start_time p, p_end_time p)
     , List.minmax (map n_frequency nd)
     , List.minmax (map n_amplitude nd)
     )

-- * Parsing

ptpf_from_plain :: Plain.Ptpf -> Ptpf
ptpf_from_plain ptpf =
  let to_f64 = Math.Convert.float_to_double
      to_node k (x, y, z) = Node k (to_f64 x) (to_f64 y) (to_f64 z)
      to_seq p =
        let k = Plain.p_id p
            n = Plain.p_size p
            st = to_f64 (Plain.p_start_time p)
            et = to_f64 (Plain.p_end_time p)
        in Seq k st et n (map (to_node k . Plain.p_node_at p) [0 .. n - 1])
  in Ptpf (Plain.ptpf_size ptpf) (map to_seq (Plain.ptpf_data_list ptpf))

parse_ptpf :: Plain.Str -> Either String Ptpf
parse_ptpf = either (Left . id) (Right . ptpf_from_plain) . Plain.ptpf_parse

-- | Load Ptpf data.
load_ptpf :: FilePath -> IO (Either String Ptpf)
load_ptpf = fmap (either (Left . id) (Right . ptpf_from_plain)) . Plain.ptpf_load

-- * Operations

-- | All 'Node's grouped into sets with equal start times.
ptpf_time_asc :: Ptpf -> [(N_Time, [Node])]
ptpf_time_asc =
  let f x = (n_time (List.head_err x), x)
  in map f
      . groupBy ((==) `on` n_time)
      . sortBy (compare `on` n_time)
      . concatMap s_data
      . p_seq

-- * Adjacencies

-- | Overlapping adjacent node pairs.
s_node_adj :: Seq -> [(Node, Node)]
s_node_adj sq = List.adj2 1 (s_data sq)

-- | Ascending sequence of adjacent nodes.
type N_Adj = [(Node, Node)]

n_adj_cmp :: (Node, Node) -> (Node, Node) -> Ordering
n_adj_cmp = compare `on` (n_time . fst)

{- | If the time difference between two adjacent nodes is less than
/dt/, remove the interior node so that (i,j),(j,k) becomes (i,k)
and the process recurs.
-}
n_adj_reduce :: N_Time -> [(Node, Node)] -> [(Node, Node)]
n_adj_reduce dt adj =
  let dur_f (n0, n1) = n_time n1 - n_time n0
  in case adj of
      (n0, n1) : (_, n2) : adj' ->
        if dur_f (n0, n1) < dt
          then n_adj_reduce dt ((n0, n2) : adj')
          else (n0, n1) : n_adj_reduce dt ((n1, n2) : adj')
      _ -> adj

p_node_adj :: Ptpf -> N_Adj
p_node_adj p =
  let sq = p_seq p
      nd = map s_node_adj sq
  in foldr (O.mergeBy n_adj_cmp) [] nd

-- | Drop nodes pairs that /end/ before indicated time.
p_node_adj_from :: N_Time -> N_Adj -> N_Adj
p_node_adj_from tm nd =
  case nd of
    [] -> []
    (_, Node _ t1 _ _) : nd' -> if t1 < tm then p_node_adj_from tm nd' else nd

{- | Take nodes pairs that begin on or before and end after the indicated time.
This discards pairs that both begin and end before the indicated time.
-}
p_node_adj_until :: N_Time -> N_Adj -> N_Adj
p_node_adj_until tm nd =
  case nd of
    [] -> []
    x : nd' ->
      let (Node _ t0 _ _, Node _ t1 _ _) = x
      in if t0 <= tm
          then
            if tm < t1
              then x : p_node_adj_until tm nd'
              else p_node_adj_until tm nd'
          else []

{- | Given ascending time locations, scan list of ascending node
windows and form groups spanning for each indicated time.
-}
p_node_adj_win :: [N_Time] -> N_Adj -> [N_Adj]
p_node_adj_win tm nd =
  case tm of
    [] -> []
    t0 : tm' ->
      let nd' = p_node_adj_from t0 nd
      in p_node_adj_until t0 nd' : p_node_adj_win tm' nd'

-- | 'n_interp' of 'p_node_adj_win'.
p_node_win_interp :: [N_Time] -> N_Adj -> [[Node]]
p_node_win_interp tm nd =
  let wn = p_node_adj_win tm nd
  in zipWith (\t w -> mapMaybe (n_interp t) w) tm wn
