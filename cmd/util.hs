import Data.List {- base -}
import Data.Maybe {- base -}
import System.Environment {- base -}
import System.FilePath {- filepath -}
import Text.Printf {- base -}

import qualified Data.Map as Map {- containers -}

import qualified Music.Theory.Array.Csv as Array.Csv {- hmt-base -}
import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Math as Math {- hmt-base -}
import qualified Music.Theory.Show as Show {- hmt-base -}
import qualified Music.Theory.Tuple as Tuple {- hmt-base -}

import qualified Graphics.Ps as Ps {- hps -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Plot as Plot {- hsc3-plot -}

import qualified Data.Cg.Minus as Cg {- hcg-minus -}

import qualified Sound.Analysis.Spear.Ptpf as Ptpf {- hspear -}
import qualified Sound.Analysis.Spear.Ptpf.Gz as Gz {- hspear -}
import qualified Sound.Analysis.Spear.Ptpf.Plain as Plain {- hspear -}
import qualified Sound.Analysis.Spear.Ptpf.Reduce as Reduce {- hspear -}

type R = Double

-- * Load

load_p :: FilePath -> IO Ptpf.Ptpf
load_p = fmap (either (error "load_p") id) . Gz.load_ptpf_maybe_gz

-- * Pp

-- | (precision,cps_f,amp_f)
type Pp_Opt = (Int, Float -> Float, Float -> Float)

node_pp_opt :: Pp_Opt -> Plain.Node -> String
node_pp_opt (precision, cps_f, amp_f) (tm, cps, amp) =
  let f_pp = Show.float_pp precision
  in intercalate "," (map f_pp [tm, cps_f cps, amp_f amp])

node_pp_w :: Int -> Ptpf.Node -> [String]
node_pp_w w (Ptpf.Node k tm fr am) = [show k, show tm, Show.double_pp w fr, Show.double_pp w (Sc3.ampDb am)]

node_pp :: Ptpf.Node -> String
node_pp (Ptpf.Node _ tm fr am) = printf "%.3f %.3f %.3f" tm fr am

-- * At

{- | Print at

> let fn = "/home/rohan/uc/guild-psaltery/txt.gz/I/123.6.txt.gz"
> print_at fn (True,2.0,5)
-}
print_at :: FilePath -> (Bool, Ptpf.N_Time, Int) -> IO ()
print_at fn (hdr_p, tm, w) = do
  p <- load_p fn
  let nd = Ptpf.p_interp tm p
      nd' = map (node_pp_w w) nd
      hdr = if hdr_p then Just ["SEQ-ID", "TIME", "FREQ(HZ)", "AMPL(DB)"] else Nothing
      csv = Array.Csv.csv_table_pp id (True, ',', False, Array.Csv.Csv_Align_Left) (hdr, nd')
  putStrLn csv

-- * Draw

type Draw_Opt = (R, R, R, R) -- (time-scale/*,threshold/db,diff/mnn,diff/db)

{- | Db to Greyscale

>>> import Data.Ratio
>>> map (db_to_greyscale (-90)) [-90,-45,-30,0] == [0,1%2,2%3,1]
True
-}
db_to_greyscale :: (Fractional a, Ord a) => a -> a -> a
db_to_greyscale th db =
  if db < th
    then 0
    else
      if db >= 0
        then 1
        else 1 - (negate db / negate th)

{-
-- > map (round . db_to_midi (-90)) [-90,-45,-30,0] == [0,64,85,127]
db_to_midi :: (Fractional a, Ord a) => a -> a -> a
db_to_midi th = (* 127) . db_to_greyscale th
-}

t3_midi_lin :: R -> Tuple.T3 R -> Maybe (Tuple.T3 R)
t3_midi_lin db (tm, fr, am) =
  let db' = Sc3.ampDb am
  in if db' > db then Just (tm, Sc3.cpsMidi fr, db_to_greyscale db db') else Nothing

to_t3_midi_lin :: R -> Ptpf.Ptpf -> [[Tuple.T3 R]]
to_t3_midi_lin db =
  let f = mapMaybe (t3_midi_lin db) . Reduce.seq_to_t3
  in map f . Ptpf.p_seq

ptpf_bbox :: R -> Ptpf.Ptpf -> Ps.BBox
ptpf_bbox n p =
  let ((t0, t1), (f0, f1), _) = Ptpf.p_extent p
  in Ps.BBox
      { Ps.llx = floor (t0 * n)
      , Ps.lly = floor (Sc3.cpsMidi f0)
      , Ps.urx = ceiling (t1 * n)
      , Ps.ury = ceiling (Sc3.cpsMidi f1)
      }

cline :: R -> ((R, R, R), (R, R, R)) -> Ps.Image
cline n ((t0, f0, a0), (t1, f1, _)) =
  let t0' = t0 * n
      t1' = t1 * n
      gs = (Ps.greyGs a0) {Ps.gs_line_width = 0.2}
  in Ps.Stroke gs (Ps.MoveTo (Cg.Pt t0' f0) <> Ps.LineTo (Cg.Pt t1' f1))

{- | Draw

> let fn = "/home/rohan/uc/guild-psaltery/txt.gz/I/123.6.txt.gz"
> draw fn (2.5,-90,0.05,0.5)
-}
draw :: FilePath -> Draw_Opt -> IO ()
draw fn opt = do
  let (tm_scale, db_threshold, mnn_diff, db_diff) = opt
  p <- load_p fn
  let txy = to_t3_midi_lin db_threshold p
      f =
        foldl Ps.Over Ps.Empty
          . map (cline tm_scale)
          . List.adj2 1
          . Reduce.t3_plain_reduce (mnn_diff, db_diff)
      xy = map f txy
      bbox = (ptpf_bbox tm_scale p)
  print bbox
  Ps.eps (fn <.> "eps") bbox (foldl Ps.Over Ps.Empty xy)

-- * Info

minmaxmean :: (Floating t, Ord t) => [t] -> (t, t, t)
minmaxmean x = let (l, r) = List.minmax x in (l, r, Math.ns_mean_of_list x)

minmaxmean' :: (Floating t, Ord t) => [(t, t, t)] -> (t, t, t)
minmaxmean' x =
  ( minimum (map (\(n, _, _) -> n) x)
  , maximum (map (\(_, n, _) -> n) x)
  , Math.ns_mean_of_list (map (\(_, _, n) -> n) x)
  )

t3_map :: (t -> u) -> (t, t, t) -> (u, u, u)
t3_map f (p, q, r) = (f p, f q, f r)

{- | Print info

> let fn = "/home/rohan/uc/guild-psaltery/txt.gz/I/123.6.txt.gz"
> print_info fn
-}
print_info :: FilePath -> IO ()
print_info fn = do
  p <- load_p fn
  let sq = Ptpf.p_seq p
      du = map Ptpf.s_duration sq
      msec :: Double -> Int
      msec = round . (*) 1000
      du' = let (l, r, m) = minmaxmean du in (msec l, msec r, msec m)
  print ("# partials", Ptpf.p_partials p)
  print ("# seq", length sq)
  print ("# nodes", Ptpf.p_nodes p)
  print ("time/sec", (Ptpf.p_start_time p, Ptpf.p_end_time p))
  print ("frequency-mmm/hz", minmaxmean' (map (Ptpf.s_summarise minmaxmean Ptpf.n_frequency) sq))
  print ("amplitude-mmm/db", t3_map Sc3.ampDb (minmaxmean' (map (Ptpf.s_summarise minmaxmean Ptpf.n_amplitude) sq)))
  print ("seq-dur/msec", du')

-- * Pgm

{- | Freq to bin

>>> map (freq_to_bin 48000 16) [0,800 .. 24000]
[0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,9,9,10,10,11,11,12,12,13,13,14,14,15,15,16]
-}
freq_to_bin :: R -> R -> R -> Int
freq_to_bin sr nbins x =
  let sz = (sr / 2) / nbins
  in round (x / sz)

{- | Fmidi to bin

>>> map (fmidi_to_bin 48000 512) [48 .. 72]
[178,181,185,189,193,196,200,204,207,211,215,219,222,226,230,233,237,241,244,248,252,256,259,263,267]
-}
fmidi_to_bin :: R -> R -> R -> Int
fmidi_to_bin sr nbins x =
  let mx = Sc3.cpsMidi (sr / 2)
      sz = mx / nbins
  in round (x / sz)

gs_8bit :: R -> Int
gs_8bit = floor . (* 255)

{-
gs_16bit :: R -> Int
gs_16bit = floor . (* 65535)
-}

node_pixel :: (Bool, R) -> Ptpf.Node -> (Int, Int)
node_pixel (lin, nbins) (Ptpf.Node _ _ fr am) =
  ( if lin
      then fmidi_to_bin 48000 nbins (Sc3.cpsMidi fr)
      else freq_to_bin 48000 nbins fr
  , gs_8bit (db_to_greyscale (-60) (Sc3.ampDb am))
  )

column_unfold_m :: Int -> Map.Map Int Int -> [Int]
column_unfold_m nr c = let f i = maybe 255 id (Map.lookup i c) in map f [0 .. nr - 1]

{- | Pgm

> let fn = "/home/rohan/uc/guild-psaltery/txt.gz/I/123.6.txt.gz"
> pgm fn (True,20.0,512)
-}
pgm :: FilePath -> (Bool, R, R) -> IO ()
pgm fn (lin, tm, nbins) = do
  p <- load_p fn
  let nbins' = floor nbins
      et = Ptpf.p_end_time p
      tms = [0, tm .. et]
      win = Ptpf.p_node_win_interp tms (Ptpf.p_node_adj p)
      tbl = map (column_unfold_m nbins' . Map.fromList . map (node_pixel (lin, nbins))) win
      tbl' = reverse (transpose tbl)
      img =
        [ ["P2"]
        , [show (length tms), show nbins']
        , ["255"]
        ]
          ++ map (map show) tbl'
  mapM_ (putStrLn . unwords) img

-- * Plot, Xy

type XY = (R, R) -- (time,cps|mnn)
type XY_Draw_Opt = (String, R, Int) -- (cps|midi,threshold/db,redact/n)

freq_f :: String -> (R -> R)
freq_f s =
  case s of
    "cps" -> id
    "midi" -> Sc3.cps_to_midi
    _ -> error "freq_f"

node_xy :: XY_Draw_Opt -> Ptpf.Node -> Maybe XY
node_xy (fr_f, db, _) (Ptpf.Node _ tm fr am) =
  if Sc3.amp_to_db am > db
    then Just (tm, freq_f fr_f fr)
    else Nothing

{- | Every nth

>>> every_nth 4 ['a' .. 'z']
"dhlptx"
-}
every_nth :: Int -> [t] -> [t]
every_nth n =
  let f _ [] = []
      f k (x : xs) = if k <= 1 then x : every_nth n xs else f (k - 1) xs
  in f n

load_xy :: FilePath -> XY_Draw_Opt -> IO [XY]
load_xy fn opt =
  let (_, _, n) = opt
      f = return . every_nth n . concatMap (mapMaybe (node_xy opt) . Ptpf.s_data) . Ptpf.p_seq
  in load_p fn >>= f

{- | Plot Xy

> let fn = "/home/rohan/uc/guild-psaltery/txt.gz/I/123.6.txt.gz"
> plot_xy fn ("midi",-90,8)
-}
plot_xy :: FilePath -> XY_Draw_Opt -> IO ()
plot_xy fn opt = do
  xy <- load_xy fn opt
  Plot.plot_p2_pt [xy]

-- * T3

type T3_Draw_Opt = (R, R, R) -- (threshold/db,diff/mnn,diff/db)

t3_midi_db :: R -> Tuple.T3 R -> Maybe (Tuple.T3 R)
t3_midi_db db (tm, fr, am) =
  let db' = Sc3.amp_to_db am
  in if db' > db then Just (tm, Sc3.cps_to_midi fr, db') else Nothing

{-
load_t3 :: FilePath -> IO [[Tuple.T3 R]]
load_t3 fn = load_p fn >>= return . map Reduce.seq_to_t3 . Ptpf.p_seq
-}

load_t3_midi_db :: FilePath -> R -> IO [[Tuple.T3 R]]
load_t3_midi_db fn db =
  let f = mapMaybe (t3_midi_db db) . Reduce.seq_to_t3
  in load_p fn >>= return . map f . Ptpf.p_seq

{- | Plot T3

> let fn = "/home/rohan/uc/guild-psaltery/txt.gz/I/123.6.txt.gz"
> plot_t3 fn (-90,0.25,2.5)
-}
plot_t3 :: FilePath -> T3_Draw_Opt -> IO ()
plot_t3 fn opt = do
  let (db_threshold, mnn_diff, db_diff) = opt
      f (tm, mnn, _) = (tm, mnn)
  txy <- load_t3_midi_db fn db_threshold
  let xy = map (map f . Reduce.t3_plain_reduce (mnn_diff, db_diff)) txy
  Plot.plot_p2_ln xy

-- * Seq

node_cmp :: Plain.Node -> Plain.Node -> Ordering
node_cmp (t0, _, _) (t1, _, _) = compare t0 t1

p_node_seq :: Plain.Partial -> [Plain.Node]
p_node_seq p = map (Plain.p_node_at p) [0 .. Plain.p_size p - 1]

ptpf_node_seq :: Plain.Ptpf -> [Plain.Node]
ptpf_node_seq = List.merge_set_by node_cmp . map p_node_seq . Plain.ptpf_data_list

print_nodes :: Pp_Opt -> Float -> FilePath -> IO ()
print_nodes pp_opt db_limit fn = do
  ptpf <- Plain.ptpf_load_err fn
  let limit_f (_, _, amp) = Sc3.amp_to_db amp > db_limit
      n = filter limit_f (ptpf_node_seq ptpf)
      l = map (node_pp_opt pp_opt) n
  putStrLn (unlines l)

seg_cmp :: Plain.Seg -> Plain.Seg -> Ordering
seg_cmp ((t0, _, _), _) ((t1, _, _), _) = compare t0 t1

seg_pp :: Pp_Opt -> Plain.Seg -> String
seg_pp (precision, cps_f, amp_f) ((tm0, cps0, amp0), (tm1, cps1, amp1)) =
  let f_pp = Show.float_pp precision
  in intercalate "," (map f_pp [tm0, cps_f cps0, amp_f amp0, tm1, cps_f cps1, amp_f amp1])

p_seg_seq :: Plain.Partial -> [Plain.Seg]
p_seg_seq p = map (Plain.p_seg_at p) [0 .. Plain.p_size p - 2]

ptpf_seg_seq :: Plain.Ptpf -> [Plain.Seg]
ptpf_seg_seq = List.merge_set_by seg_cmp . map p_seg_seq . Plain.ptpf_data_list

-- > let fn = "/home/rohan/tn/music-ts/harmonic/timing/text/timing.L.ptpf.text"
-- > print_segments (6,Sc3.cps_to_midi,Sc3.amp_to_db) fn
print_segments :: Pp_Opt -> Float -> FilePath -> IO ()
print_segments pp_opt db_limit fn = do
  ptpf <- Plain.ptpf_load_err fn
  let limit_f ((_, _, amp1), (_, _, amp2)) =
        Sc3.amp_to_db amp1 > db_limit
          || Sc3.amp_to_db amp2 > db_limit
      n = filter limit_f (ptpf_seg_seq ptpf)
      l = map (seg_pp pp_opt) n
  putStrLn (unlines l)

-- > let fn = "/home/rohan/tn/music-ts/harmonic/timing/text/timing.L.ptpf.text"
-- > print_seq 35 fn
print_seq :: Int -> FilePath -> IO ()
print_seq k fn = do
  p <- load_p fn
  let mmm xs = let (l, r) = List.minmax xs in (l, r, Math.ns_mean_of_list xs)
  case filter ((==) k . Ptpf.s_identifier) (Ptpf.p_seq p) of
    [sq'] -> do
      print ("seq-id", Ptpf.s_identifier sq')
      print ("# nodes", Ptpf.s_nodes sq')
      print ("start-time/sec", Ptpf.s_start_time sq')
      print ("end-time/sec", Ptpf.s_end_time sq')
      print ("freq/hz", mmm (map Ptpf.n_frequency (Ptpf.s_data sq')))
      mapM_ (putStrLn . node_pp) (Ptpf.s_data sq')
    _ -> error "print_seq"

parse_pp_opt :: String -> String -> String -> Pp_Opt
parse_pp_opt prec cps amp =
  let cps_f = case cps of
        "cps" -> id
        "mnn" -> Sc3.cps_to_midi
        _ -> error "parse_pp_opt?"
      amp_f = case amp of
        "amp" -> id
        "db" -> Sc3.amp_to_db
        "vel" -> (* 127.0)
        _ -> error "parse_pp_opt?"
  in (read prec, cps_f, amp_f)

print_f :: String -> Pp_Opt -> Float -> FilePath -> IO ()
print_f x =
  case x of
    "nodes" -> print_nodes
    "segments" -> print_segments
    _ -> error "print_f?"

-- * Main

help :: [String]
help =
  [ "hspear-util cmd arg"
  , "  at time/sec hdr:bool precision:int file-name"
  , "  draw eps file-name:string time-*/1.0 threshold:db/-45 mnn-diff/0.25 db-diff/2.5"
  , "  info file-name"
  , "  pgm file-name log|lin time-step/sec #-frequency-bins"
  , "  plot xy file-name:string cps|midi threshold:db/-45 redact:int/8"
  , "  plot ln file-name:string threshold:db/-45 mnn-diff/0.25 db-diff/2.5"
  , "  print nodes|segments precision:int cps|midi amp|db|vel db-limit file-name:string"
  , "  seq seq-id file-name"
  , ""
  , "    hdr = print header = t | f"
  , ""
  , "  at: print linear interpolation of sequence data at indicated time"
  ]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["at", hdr, tm, prec, fn] -> print_at fn (hdr == "t", read tm, read prec)
    ["draw", "eps", fn, tm, db, mnn_diff, db_diff] -> draw fn (read tm, read db, read mnn_diff, read db_diff)
    ["info", fn] -> print_info fn
    ["pgm", fn, "lin", tm, nbins] -> pgm fn (True, read tm, read nbins)
    ["pgm", fn, "log", tm, nbins] -> pgm fn (False, read tm, read nbins)
    ["plot", "xy", fn, fmt, db, n] -> plot_xy fn (fmt, read db, read n)
    ["plot", "ln", fn, db, mnn_diff, db_diff] -> plot_t3 fn (read db, read mnn_diff, read db_diff)
    ["print", mode, prec, cps, amp, db_limit, fn] -> (print_f mode) (parse_pp_opt prec cps amp) (read db_limit) fn
    ["seq", k, fn] -> print_seq (read k) fn
    _ -> mapM_ putStrLn help
