# at

Print linear interpolation of `PTPF` sequence data at indicated time
to indicated precision.

~~~~
$ hspear-util at t 25.0 5 ~/tn/music-ts/harmonic/timing/timing.L.ptpf.text
SEQ-ID,TIME,  FREQ(HZ), AMPL(DB)
     6,25.0, 165.70861,-33.82487
    15,25.0, 647.42994,-34.72450
    16,25.0,1202.52166,-55.93682
    18,25.0, 432.00998,-33.01348
    19,25.0, 971.03340,-43.32005
    20,25.0,  64.50688,-54.34879
    21,25.0, 686.29527,-36.60106
    22,25.0,1294.97928,-38.47979
    23,25.0, 216.11762,-41.49003
    24,25.0, 514.74990,-38.95979
    25,25.0,1731.42840,-50.69554
    26,25.0,2401.65448,-63.97393
    28,25.0, 128.79450,-34.48001
    29,25.0, 386.24961,-41.44031
    30,25.0, 863.75272,-34.04736
    31,25.0,1943.83368,-55.47492
    32,25.0, 771.90411,-54.54574
    33,25.0, 485.73707,-39.44681
    34,25.0,3025.36102,-62.85378
    35,25.0,1029.24017,-46.69958
    36,25.0,1512.95927,-59.96776
    37,25.0, 899.47864,-59.15437
    39,25.0, 321.93600,-37.89232
    41,25.0, 575.20374,-64.46199
    42,25.0, 345.12099,-39.31147
$
~~~~

# draw

Generate `EPS` drawing of `PTPF` data using [hps](?t=hps).

~~~~
$ hspear-util draw -h
hspear-util draw eps file-name:string time-*/1.0 threshold:db/-45 mnn-diff/0.25 db-diff/2.5
$
~~~~

# info

Print summary of data at `PTPF` file.

~~~~
$ hspear-util info ~/tn/music-ts/harmonic/timing/timing.L.ptpf.text
("# partials",1694)
("# seq",1694)
("# nodes",965313)
("time/sec",(0.337347,604.738159))
("frequency-mmm/hz",(10.996987,5056.04541,472.0877646489807))
("amplitude-mmm/db",(-Infinity,-23.09344929955866,-40.134584384027576))
("seq-dur/msec",(37,359612,7195))
$
~~~~

# pgm

Generate `PGM` drawing of `PTPF` data.

~~~~
$ hspear-util pgm -h
hspear-util pgm file-name log|lin time-step/sec #-frequency-bins
$ hspear-util pgm timing.L.ptpf.text lin 0.25 1024 > timing.L.ptpf.pgm
~~~~

# plot

Make `SVG` drawings of `PTPF` data using [hsc3-plot](?t=hsc3-plot).

~~~~
$ hspear-util plot -h
hspear-util plot xy file-name:string cps|midi threshold:db/-45 redact:int/8
hspear-util plot ln file-name:string threshold:db/-45 mnn-diff/0.25 db-diff/2.5
$
~~~~

# plot/xy

~~~~
$ hspear-util plot xy timing.L.ptpf.text midi -40 16
~~~~

# plot/ln

~~~~
$ hspear-util plot ln timing.L.ptpf.text -40 0.25 2.5
~~~~

# seq

Print indicated sequence of `PTPF` data set.

~~~~
$ hspear-util seq 58 ~/tn/music-ts/harmonic/timing/text/timing.L.ptpf.text
("seq-id",58)
("# nodes",4)
("start-time/sec",36.183582)
("end-time/sec",36.221066)
("freq/hz",(321.702637,326.002899,324.202774))
36.184 323.103 0.011
36.196 321.703 0.011
36.209 326.003 0.013
36.221 326.003 0.000
$
~~~~
